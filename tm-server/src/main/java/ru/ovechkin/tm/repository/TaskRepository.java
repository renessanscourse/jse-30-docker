package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    @NotNull
    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public final void add(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Nullable
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Query query = entityManager.createQuery(
                "from Task where user_id = :userId and id = :id", Task.class);
        query.setParameter("userId", userId);
        query.setParameter("id", id);
        @NotNull final Task task = (Task) query.getSingleResult();
        return task;
    }

    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Query query = entityManager.createQuery(
                "from Task where user_id = :userId and name = :name", Task.class);
        query.setParameter("userId", userId);
        query.setParameter("name", name);
        @NotNull final Task task = (Task) query.getSingleResult();
        return task;
    }

    @Nullable
    @Override
    public final List<Task> findUserTasks(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final Query query = entityManager.createQuery(
                "from Task where user_id = :userId", Task.class);
        query.setParameter("userId", userId);
        if (query.getResultList() == null || query.getResultList().isEmpty()) return null;
        @Nullable final List<Task> tasks = (List<Task>) query.getResultList();
        return tasks;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @NotNull final Query query = entityManager.createQuery("delete Task where user_id = :userId");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Task removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        entityManager.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        entityManager.remove(task);
        return task;
    }


    @Nullable
    @Override
    public List<Task> getAllTasks() {
        @Nullable final Query query = entityManager.createQuery("from Task", Task.class);
        return (List<Task>) query.getResultList();
    }

    @NotNull
    @Override
    public List<Task> mergeCollection(@NotNull final List<Task> taskList) {
        for (@NotNull final Task task : taskList) {
            entityManager.persist(task);
        }
        return taskList;
    }

    @Override
    public void removeAllTasks() {
        final Query query = entityManager.createQuery("delete from Task", Task.class);
        query.executeUpdate();
        entityManager.close();
    }

}
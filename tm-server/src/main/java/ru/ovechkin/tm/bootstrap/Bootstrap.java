package ru.ovechkin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

import javax.xml.ws.Endpoint;

public final class Bootstrap {

    @NotNull
    final IServiceLocator serviceLocator = new ServiceLocator();

    @NotNull
    final IEndpointLocator endpointLocator = new EndpointLocator(serviceLocator);

    final IPropertyService propertyService = serviceLocator.getPropertyService();

    public void init() throws Exception {
        initProperty();
        serviceLocator.getEntityManagerService().init();
        initEndpoint();
    }

    private void initProperty() throws Exception {
        propertyService.init();
    }

    private void initEndpoint() {
        registry(endpointLocator.getUserEndpoint());
        registry(endpointLocator.getSessionEndpoint());
        registry(endpointLocator.getStorageEndpoint());
        registry(endpointLocator.getTaskEndpoint());
        registry(endpointLocator.getProjectEndpoint());
    }

    private void registry(final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String host = propertyService.getServiceHost();
        @NotNull final Integer port = propertyService.getServicePort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
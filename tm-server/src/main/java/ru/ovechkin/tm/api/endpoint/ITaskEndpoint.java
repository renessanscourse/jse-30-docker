package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void add(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "task", partName = "task") TaskDTO taskDTO,
            @Nullable @WebParam(name = "project", partName = "project") ProjectDTO projectDTO
    );

    @WebMethod
    void createTaskWithName(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "taskName", partName = "taskName") String taskName,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId
    );

    @WebMethod
    void createTaskWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "taskName", partName = "taskName") String taskName,
            @Nullable @WebParam(name = "taskDescription", partName = "taskDescription") String taskDescription,
            @Nullable @WebParam(name = "projectId", partName = "projectId") String projectId
    );

    @WebMethod
    @Nullable List<TaskDTO> findUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    );

    @WebMethod
    void removeAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO
    );

    @WebMethod
    TaskDTO findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    @Nullable TaskDTO findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

    @WebMethod
    @Nullable TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    );

    @WebMethod
    @Nullable TaskDTO removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") String id
    );

    @WebMethod
    @Nullable TaskDTO removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") String name
    );

}
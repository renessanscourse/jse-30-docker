package ru.ovechkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.service.IPropertyService;
import ru.ovechkin.tm.exeption.other.PropertiesInvalidException;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private final String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    public void init() throws Exception {
        final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        properties.load(inputStream);
    }

    @NotNull
    @Override
    public String getServiceHost() {
        final String propertyHost = properties.getProperty("server.host");
        if (propertyHost == null || propertyHost.isEmpty()) throw new PropertiesInvalidException();
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServicePort() {
        final String propertyPort = properties.getProperty("server.port");
        if (propertyPort == null || propertyPort.isEmpty()) throw new PropertiesInvalidException();
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getSessionSalt() {
        init();
        final String propertiesSalt = properties.getProperty("session.salt");
        if (propertiesSalt == null || propertiesSalt.isEmpty()) throw new PropertiesInvalidException();
        return propertiesSalt;
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return properties.getProperty("db.driver");
    }

    @NotNull
    @Override
    @SneakyThrows
    public String getJdbcUrl() {
        @Nullable final String type = properties.getProperty("db.type");
        if (type == null || type.isEmpty()) throw new PropertiesInvalidException();
        @Nullable final String host = properties.getProperty("db.host");
        if (host == null || host.isEmpty()) throw new PropertiesInvalidException();
        @Nullable final String port = properties.getProperty("db.port");
        if (port == null || port.isEmpty()) throw new PropertiesInvalidException();
        @Nullable final String name = properties.getProperty("db.name");
        if (name == null || name.isEmpty()) throw new PropertiesInvalidException();
        @Nullable final String timezone = properties.getProperty("db.timezone");
        if (timezone == null || timezone.isEmpty()) throw new PropertiesInvalidException();
        @NotNull final String dbUrl = String.format(
                "%s://%s:%s/%s?serverTimezone=%s",
                type, host, port, name, timezone
        );
        if (dbUrl == null || dbUrl.isEmpty()) throw new PropertiesInvalidException();
        return dbUrl;
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        final String propertiesLogin = properties.getProperty("db.login");
        if (propertiesLogin == null || propertiesLogin.isEmpty()) throw new PropertiesInvalidException();
        return propertiesLogin;
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        final String propertiesPassword = properties.getProperty("db.password");
        if (propertiesPassword == null || propertiesPassword.isEmpty()) throw new PropertiesInvalidException();
        return propertiesPassword;
    }

}
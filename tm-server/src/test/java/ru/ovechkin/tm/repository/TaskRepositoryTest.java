package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.dto.TaskDTO;

import java.util.List;

public class TaskRepositoryTest {
/*

    private final ITaskRepository taskRepository = new TaskRepository();

    private  final TasksForTest tasksForTest = new TasksForTest();

    private final TaskDTO taskDTO1 = tasksForTest.getTask1();

    private final TaskDTO taskDTO2 = tasksForTest.getTask2();

    private  final TaskDTO taskDTO3 = tasksForTest.getTask3();

    public List<TaskDTO> listOfTestTasks() {
        return tasksForTest.getListOfTestTasks();
    }

    @Before
    public void clearRepository() {
        taskRepository.clear();
        taskRepository.merge(listOfTestTasks());
    }

    @Test
    public void testAdd() {
        taskRepository.clear();
        taskRepository.add(taskDTO1.getUserId(), taskDTO1);
        Assert.assertEquals(taskDTO1, taskRepository.findById(taskDTO1.getUserId(), taskDTO1.getId()));
    }

    @Test
    public void testFindUserTasks() {
        final List<TaskDTO> userOfTaskDTOS = listOfTestTasks();
        userOfTaskDTOS.remove(taskDTO3);// убираем таск с ненужным userId
        Assert.assertEquals(userOfTaskDTOS, taskRepository.findAllUserTask(TestConst.USER_ID_USER));
    }

    @Test
    public void testRemoveAll() {
        final List<TaskDTO> taskDTOS = listOfTestTasks();
        taskDTOS.removeAll(listOfTestTasks());
        taskRepository.removeAll(TestConst.USER_ID_USER);
        Assert.assertEquals(taskDTOS, taskRepository.findAllUserTask(TestConst.USER_ID_USER));
    }

    @Test
    public void testFindById() {
        taskRepository.clear();
        taskRepository.merge(taskDTO1);
        Assert.assertEquals(taskDTO1, taskRepository.findById(taskDTO1.getUserId(), taskDTO1.getId()));
        Assert.assertNull(taskRepository.findById(taskDTO2.getUserId(), taskDTO2.getId()));
        Assert.assertNull(taskRepository.findById(taskDTO2.getUserId(), taskDTO3.getId()));
    }

    @Test
    public void testFindByIndex() {
        Assert.assertEquals(taskDTO3, taskRepository.findByIndex(taskDTO3.getUserId(), 1));
        Assert.assertNull(taskRepository.findByIndex(taskDTO3.getUserId(), 2));
    }

    @Test
    public void testFindByName() {
        Assert.assertEquals(taskDTO3, taskRepository.findByName(taskDTO3.getUserId(), taskDTO3.getName()));
        Assert.assertNull(taskRepository.findByName(taskDTO3.getUserId(), "task3.getName())"));
        Assert.assertNull(taskRepository.findByName(taskDTO2.getUserId(), taskDTO3.getName()));
    }

    @Test
    public void testRemoveById() {
        taskRepository.removeById(taskDTO1.getUserId(), taskDTO1.getId());
        Assert.assertNull(taskRepository.findById(taskDTO1.getUserId(), taskDTO1.getId()));
    }

    @Test
    public void testRemoveByIndex() {
        taskRepository.removeByIndex(taskDTO1.getUserId(), 1);
        Assert.assertNull(taskRepository.findById(taskDTO1.getUserId(), taskDTO1.getId()));
    }

    @Test
    public void testRemoveByName() {
        taskRepository.removeByName(taskDTO1.getUserId(), taskDTO1.getName());
        Assert.assertNull(taskRepository.findByName(taskDTO1.getUserId(), taskDTO1.getName()));
    }
*/

}
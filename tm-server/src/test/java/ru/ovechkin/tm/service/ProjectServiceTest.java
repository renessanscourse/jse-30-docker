package ru.ovechkin.tm.service;

import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.IProjectService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.locator.ServiceLocator;

public class ProjectServiceTest {

    final IServiceLocator serviceLocator = new ServiceLocator();

    final IProjectService projectService = new ProjectService(serviceLocator);

    @Test
    public void testAdd() {
        final SessionDTO sessionDTO = serviceLocator.getSessionService().open("admin", "admin");
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("testProjectDTO1");
        projectDTO.setDescription("testDescription");
        projectService.add(sessionDTO, projectDTO);
        serviceLocator.getSessionService().signOutByLogin("admin");
    }

    @Test
    public void testCreateWithName() {
        final SessionDTO sessionDTO = serviceLocator.getSessionService().open("admin", "admin");
        projectService.create(sessionDTO, "testCreateWithName");
        serviceLocator.getSessionService().signOutByLogin("admin");
    }

    @Test
    public void testCreateWithNameAndDescription() {
        final SessionDTO sessionDTO = serviceLocator.getSessionService().open("admin", "admin");
        projectService.create(sessionDTO, "testCreateWithDescription", "testDescription");
        serviceLocator.getSessionService().signOutByLogin("admin");
    }

    @Test
    public void testGetProjectsDto() {
        System.out.println(projectService.findUserProjects("22bd73ee-90f3-4cde-b4ca-182397d3adfa"));
    }

    @Test
    public void testRemoveAll() {
        projectService.removeAllUserProjects("22bd73ee-90f3-4cde-b4ca-182397d3adfa");
    }

    @Test
    public void testFindProjectById() {
        System.out.println(projectService.findProjectById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "36f95f88-fa67-4405-8e08-32d462ac9f27"));
    }

    @Test
    public void testFindProjectByName() {
        System.out.println(projectService.findProjectByName(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "testProjectDTO2"));
    }

    @Test
    public void testUpdateProjectById() {
        projectService.updateProjectById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "36f95f88-fa67-4405-8e08-32d462ac9f27",
                "projectChanged",
                "changedDesc");
    }

    @Test
    public void testRemoveProjectById() {
        projectService.removeProjectById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "d38040c0-3b1f-44b8-a47e-1ddb278a18c4");
    }

    @Test
    public void testRemoveProjectByName() {
        projectService.removeProjectByName(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "testProjectDTO1");
    }
}
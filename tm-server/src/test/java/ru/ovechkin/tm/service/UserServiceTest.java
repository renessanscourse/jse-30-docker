package ru.ovechkin.tm.service;

import org.junit.Test;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.locator.ServiceLocator;
import ru.ovechkin.tm.util.HashUtil;


public class UserServiceTest {

    final IUserService userService = new UserService(new ServiceLocator());

    @Test
    public void testCreate() {
        userService.create("admin", "admin", Role.ADMIN);
    }

    @Test
    public void testFindById() {
        final UserDTO userDTO = userService.findById("09c8b99a-e341-4a4e-b529-047122d71085");
        System.out.println(userDTO);
    }

    @Test
    public void testFindByLogin() {
        final UserDTO userDTO = userService.findByLogin("createFromService");
        System.out.println(userDTO);
    }

    @Test
    public void testRemoveUser() {
        final UserDTO userDTO = new UserDTO("forRemove", HashUtil.salt("user"), Role.USER);
        userDTO.setId("test");
        userService.removeUser(userDTO);
    }

    @Test
    public void testRemoveUserById() {
//        final UserDTO userDTO = userService.create("userForRemove", "forRemove");
        final UserDTO adminDTO = userService.findByLogin("adminForRemove");
        userService.removeById(adminDTO.getId(), "09c8b99a-e341-4a4e-b529-047122d71085");
    }

    @Test
    public void testRemoveUserByLogin() {
        final UserDTO userDTO = userService.create("userForRemove", "forRemove");
        final UserDTO adminDTO = userService.findByLogin("adminForRemove");
        userService.removeByLogin(adminDTO.getId(), userDTO.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        final UserDTO userDTO = userService.create("userForRemove", "forRemove");
        final UserDTO adminDTO = userService.findByLogin("adminForRemove");
        userService.lockUserByLogin(adminDTO.getId(), userDTO.getLogin());
    }

    @Test
    public void testUnLockUserByLogin() {
//        final UserDTO userDTO = userService.create("userForRemove", "forRemove");
        final UserDTO adminDTO = userService.findByLogin("adminForRemove");
        userService.unLockUserByLogin(adminDTO.getId(), "userForRemove");
    }

}
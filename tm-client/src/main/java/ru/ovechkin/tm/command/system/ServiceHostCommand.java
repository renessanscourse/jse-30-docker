package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public class ServiceHostCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SERVER_HOST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show server host";
    }

    @Override
    public void execute() {
        System.out.println("[HOST]");
        System.out.println(endpointLocator.getStorageEndpoint().getServerHostInfo());
        System.out.println("[OK]");
    }

}
